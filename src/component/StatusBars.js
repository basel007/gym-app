import React from 'react';
import {View, StatusBar} from 'react-native';

export default function StatusBars(props) {
  return (
    <View>
      <StatusBar barStyle="light-content" backgroundColor="#2196F3" />
    </View>
  );
}
