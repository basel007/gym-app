import {white, shadow, secondarytext} from './color';
import font from './fonts';
import {sm, bm} from './bordersize';

export default {
  maincontainer: {
    flex: 1,
  },

  plusbutton: {
    position: 'absolute',
    right: 10,
    bottom: 10,
  },

  activitycontainer: {
    backgroundColor: white,
    flex: 1,
    borderRadius: sm,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },

  eventcontainer: {
    margin: 20,
    borderRadius: bm,
    backgroundColor: white,
    padding: 20,
    shadowColor: shadow,
    shadowOffset: {width: 0, height: 3},
    shadowOpacity: 1,
    shadowRadius: 5,
    elevation: 3,
  },

  eventtile: {
    fontSize: font.h1,
    alignSelf: 'center',
  },

  eventtext: {
    color: secondarytext,
    marginVertical: 10,
  },

  namebodycontainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 0,
    marginVertical: 5,
    backgroundColor: white,
    shadowColor: shadow,
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 1,
    shadowRadius: 3,
    elevation: 1,
  },

  nametextinput: {
    height: 40,
    padding: 0,
    flex: 1,
    paddingLeft: 10,
  },

  buttoncontainer: {
    paddingHorizontal: 30,
    marginTop: 20,
    marginBottom: 5,
  },
};
