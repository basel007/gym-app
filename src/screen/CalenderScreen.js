import React, {useState, useEffect} from 'react';
import {Text, View, TouchableOpacity} from 'react-native';
import {Agenda} from 'react-native-calendars';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {db} from './firebase';
import moment from 'moment';
import style from '../utils/style';
import {silver} from '../utils/color';
let itemsRef = db.ref('/event');

export default function CalenderScreen(props) {
  const [items, setitems] = useState({});
  const [agenda, setAgenda] = useState([]);

  useEffect(() => {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      let agenda = Object.values(data);
      let itemsObj = {};
      agenda.map(record => {
        let dateKey = moment(record.item.date).format('YYYY-MM-DD');
        if (!itemsObj.hasOwnProperty(dateKey)) {
          itemsObj[dateKey] = [];
        }
        itemsObj[dateKey].push({
          name: record.item.name,
        });
      });
      setitems(itemsObj);
      setAgenda(agenda);
    });
  }, []);

  const loadItems = day => {
    setTimeout(() => {
      for (let i = -15; i < 15; i++) {
        const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        const strTime = timeToString(time);
        if (!items[strTime]) {
          items[strTime] = [];
          const numItems = Math.floor(Math.random() * 5);
          for (let j = 0; j < numItems; j++) {
            items[strTime].push({});
          }
        }
      }
      const newItems = {};
      Object.keys(items).forEach(key => {
        newItems[key] = items[key];
      });
      setitems(items);
    }, 1000);
  };

  const renderItem = item => {
    return (
      <TouchableOpacity style={style.activitycontainer}>
        <Text>{item && item.hasOwnProperty('name') ? item.name : null}</Text>
      </TouchableOpacity>
    );
  };

  const rowHasChanged = (r1, r2) => {
    return r1.name !== r2.name;
  };

  const timeToString = time => {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  };
  return (
    <View style={style.maincontainer}>
      <Agenda
        items={items}
        loadItemsForMonth={e => loadItems(e)}
        selected={'2020-03-25'}
        renderItem={e => renderItem(e)}
        rowHasChanged={(v, x) => rowHasChanged(v, x)}
      />
      <TouchableOpacity
        onPress={() => props.navigation.navigate('AddEventScreen')}
        style={style.plusbutton}>
        <AntDesign name="pluscircle" size={40} color={silver} />
      </TouchableOpacity>
    </View>
  );
}
