import React, {useState} from 'react';
import {View, Text, TextInput} from 'react-native';
import StatusBars from '../component/StatusBars';
import DatePicker from 'react-native-datepicker';
import Toast from 'react-native-simple-toast';
import {Toolbar, Button} from 'react-native-material-ui';
import {db} from './firebase';
import Moment from 'moment';
import style from '../utils/style';
let addItem = item => {
  db.ref('/event').push({
    item,
  });
};
export default function AddEventScreen(props) {
  const [name, setFName] = useState('');
  const [error] = useState('');
  const [date, setDate] = useState(new Date());
  const [errors, setError] = useState({});
  const [validationerror, setValidation] = useState(false);

  const validateForm = () => {
    let formValid = true;
    let errors = {};
    if (date === '') {
      formValid = false;
      errors['date'] = '*Date is required';
    } else {
      delete errors['date'];
    }
    if (name === '') {
      formValid = false;
      errors['name'] = '*Event Name  is required';
    } else {
      delete errors['name'];
    }
    setError(errors);
    setValidation(true);
    return formValid;
  };

  const onSubmithandle = () => {
    try {
      if (validateForm()) {
        Moment.locale('en');
        const item = {
          name: name,
          date: Moment(date).format(),
        };
        addItem(item);
        Toast.show('Event Succesfully Added');
        props.navigation.goBack();
      }
    } catch (error) {
      console.log('Error', data);
    }
  };
  return (
    <View>
      <StatusBars />
      <Toolbar
        leftElement="arrow-back"
        centerElement="Event"
        onLeftElementPress={() => {
          props.navigation.goBack();
        }}
      />
      <View style={style.eventcontainer}>
        <View>
          <Text style={style.eventtile}>Event Details</Text>
          <>
            <Text style={style.eventtext}>Name</Text>
            <View style={style.namebodycontainer}>
              <TextInput
                onChangeText={text => setFName(text)}
                value={name}
                style={style.nametextinput}
                placeholder={'Event Name'}
              />
            </View>
            {validationerror != false && (
              <Text style={{marginLeft: 5}}>{errors['name']}</Text>
            )}
          </>
          <>
            <Text style={style.eventtext}>Date</Text>
            <View style={style.namebodycontainer}>
              <DatePicker
                style={{width: 200}}
                date={date}
                mode="date"
                placeholder="select date"
                format="YYYY-MM-DD"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 0,
                    marginLeft: 0,
                  },
                  dateInput: {
                    borderWidth: 0,
                  },
                }}
                onDateChange={date => setDate(date)}
              />
            </View>
            {validationerror != false && (
              <Text style={{marginLeft: 5}}>{errors['date']}</Text>
            )}
          </>
          <>
            <View style={style.buttoncontainer}>
              <Button
                style={{
                  container: {
                    borderRadius: 40,
                  },
                }}
                raised
                primary
                text="Submit"
                onPress={e => onSubmithandle(e)}
              />
            </View>
          </>
        </View>
      </View>
    </View>
  );
}
